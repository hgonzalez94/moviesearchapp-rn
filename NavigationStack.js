import {createStackNavigator, createAppContainer} from 'react-navigation';
import QueryScreen from 'app/components/QueryScreen'

const MainNavigator = createStackNavigator({
    Home: {screen: QueryScreen},
});

const App = createAppContainer(MainNavigator);

export default App;