Move Search App Test Project


# Installation & Setup

- Setup React Native following these steps (Building projects with native code. Not Expo)
    https://facebook.github.io/react-native/docs/getting-started

- Run with react native cli:

iOS:

(be sure to have xcode command line tools installed as well as an available sim)

```
react-native run-ios
```

Android:

(be sure to have an android emulator running)

```
react-native run-android
```

# Sample Movie DB Search Response

```
{
    "page": 1,
    "results": [
        {
            "adult": false,
            "backdrop_path": "/amYkOxCwHiVTFKendcIW0rSrRlU.jpg",
            "genre_ids": [
                12,
                28,
                878
            ],
            "id": 1891,
            "original_language": "en",
            "original_title": "The Empire Strikes Back",
            "overview": "The epic saga continues as Luke Skywalker, in hopes of defeating the evil Galactic Empire, learns the ways of the Jedi from aging master Yoda. But Darth
Vader is more determined than ever to capture Luke. Meanwhile, rebel leader Princess Leia, cocky Han Solo, Chewbacca, and droids C-3PO and R2-D2 are thrown into various stages of capture, betrayal and despair.",
            "popularity": 17.936,
            "poster_path": "/9SKDSFbaM6LuGqG1aPWN3wYGEyD.jpg",
            "release_date": "1980-05-20",
            "title": "The Empire Strikes Back",
            "video": false,
            "vote_average": 8.3,
            "vote_count": 9290
        }
    ],
    "total_pages": 1,
    "total_results": 1
}
```


### Note

- Movie db api key is exposed publicly on purpose. Use as long as its available.
- Various optimizations can be made, especially for app wide state management. The purpose
of this exercise was for a simple implementation to spur discussion.