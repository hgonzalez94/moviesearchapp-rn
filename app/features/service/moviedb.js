import {Api, SEARCH_MOVIES} from "./api";

/**
 * Searches the movie db for a query
 * @param query
 * @returns {Promise<*>|*|PromiseLike<T>|Promise<T>}
 */
export function search(query) {
    const params = {
        query: query,
        api_key: Api.apiKey
    };
    const url = Api.urlForRoute(SEARCH_MOVIES, params);
    return Api.getRequest(url);
}