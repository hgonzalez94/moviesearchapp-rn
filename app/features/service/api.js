export const SEARCH_MOVIES = 'search/movie';

export class Api {
    static POST = 'POST';
    static GET = 'GET';
    static PUT = 'PUT';
    static UPDATE = 'UPDATE';
    static DELETE = 'DELETE';

    static baseUrl = 'https://api.themoviedb.org/3/';

    static apiKey = '77844a70eb81040b0b76f7e727bacbce';

    static getRequestHeaders() {
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        };
    }

    static urlForRoute(route, params) {
        //https://api.themoviedb.org/3/search/movie?query=starwars&api_key=77844a70eb81040b0b76f7e727bacbce
        let url = this.baseUrl + route;
        if (params !== undefined && params !== null)
            url = this.urlWithParams(url, params);
        return url;
    }

    /**
     * request headers specific for api's
     */
    requestHeaders() {}

    /**
     * @param url
     * @param params
     * @returns a url encoded with parameters for get requests
     */
    static urlWithParams(url, params) {
        let queryParams = Object.keys(params)
            .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
            .join('&');
        return url + (url.indexOf('?') === -1 ? '?' : '&') + queryParams;
    }

    /**
     * get request
     * @param url
     * @param body
     * @returns {*}
     */
    static getRequest(url) {
        return this.executeRequestOfType(url, Api.GET, null);
    }

    /**
     * post request
     * @param url
     * @param body
     * @returns {*}
     */
    static postRequest(url, body) {
        return this.executeRequestOfType(url, Api.POST, null);
    }

    /**
     * put request
     * @param url
     * @param body
     * @returns {*}
     */
    static putRequest(url, body) {
        return this.executeRequestOfType(url, Api.PUT, null);
    }

    /**
     * update request
     * @param url
     * @param body
     * @returns {*}
     */
    static updateRequest(url, body) {
        return this.executeRequestOfType(url, Api.UPDATE, null);
    }

    /**
     * delete request
     * @param url
     * @param body
     * @returns {*}
     */
    static deleteRequest(url, body) {
        return this.executeRequestOfType(url, Api.DELETE, null);
    }

    static executeRequestOfType(url, method, body) {
        return new Promise(function (resolve, reject) {
            const requestParams = {
                method: method,
                headers: Api.getRequestHeaders(),
                body: body
            };
            return fetch(url, requestParams)
                .then((response) => {
                    return response.json();
                })
                .then((responseData) => {
                    return responseData;
                })
                .then((data) => {
                    if (data !== undefined) resolve(data);
                    else return (data);
                })
                .catch((error) => {
                    reject(error);
                })
        });
    }
}