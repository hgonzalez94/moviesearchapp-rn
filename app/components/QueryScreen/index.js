import React, {Component} from 'react';
import {Alert, View, TextInput, StyleSheet, Button, SectionList, TouchableOpacity, Text} from "react-native";
import {search} from "app/features/service/moviedb";

type Props = {};
export default class QueryScreen extends Component<Props> {
    static navigationOptions = {
        title: 'Search'
    };

    constructor(props) {
        super(props);
        this.state = {query: '', activeResults: null};
    }

    sectionData() {
        let data = this.state.activeResults != null ? this.state.activeResults.results : [];
        return [
            { title: 'Movies', key: 's0', selected: false, data: data },
        ];
    }

    selectedRow(index, section) { }

    rowViewForIndex(index, section) {
        const item = section.data[index];
        return (
            <View style={styles.rowContainer}>
                <Text style={styles.rowTitle}>{item.title}</Text>
                <Text style={styles.rowDescription}>{item.overview}</Text>
            </View>
        );
    }

    selectedSection(section) { }

    sectionView(section) {
        const sectionData = section.data;
        const title = sectionData.length > 0 ? section.title : '';
        return (
            <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>{title}</Text>
            </View>
        );
    }

    searchError(e) {
        Alert.alert(
            'Search Error',
            e.message,
            [
                { text: 'OK', onPress: () => console.log('dismiss error log') }
            ],
            { cancelable: false }
        );
    }

    searchMovieDb() {
        if (this.state.query !== '') {
            search(this.state.query).then(response => {
                if (response.error === undefined)
                    this.setState({activeResults: response});
                else
                    this.searchError(response.error);
            }).catch(e => {
                this.searchError(e);
            });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    style={styles.textInput}
                    textAlign={'center'}
                    placeholder={'Search for movie'}
                    onChangeText={(text) => this.setState({query: text})} />
                <Button
                    onPress={() => this.searchMovieDb()}
                    title={'Search'}
                    color={'#841584'} />
                <SectionList
                    style={styles.listContainer}
                    stickySectionHeadersEnabled={false}
                    sections={this.sectionData()}
                    keyExtractor={(item, index) => index}
                    extraData={this.state}
                    renderItem={({ item, index, section }) => (
                        <TouchableOpacity onPress={() => this.selectedRow(index, section)}>
                            {this.rowViewForIndex(index, section)}
                        </TouchableOpacity>
                    )}
                    renderSectionHeader={({ section }) => (
                        <TouchableOpacity onPress={() => this.selectedSection(section)}>
                            {this.sectionView(section)}
                        </TouchableOpacity>
                    )}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    textInputContainer: {
        flexDirection: 'row',
    },
    textInput: {
        height: 40,
        width: '100%'
    },
    listContainer: {
        width: '100%',
        flex: 1
    },
    sectionContainer: {
        height: 40,
        width: '100%',
        marginBottom: '2.5%'
    },
    sectionTitle: {
        fontWeight: '900',
        marginLeft: '2.5%',
        fontSize: 32
    },
    rowContainer: {
        width: '100%',
    },
    rowTitle: {
        marginLeft: '2.5%',
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: '2.5%'
    },
    rowDescription: {
        marginLeft: '2.5%',
        fontWeight: '200',
        fontSize: 20,
        marginBottom: '5%'
    }
});